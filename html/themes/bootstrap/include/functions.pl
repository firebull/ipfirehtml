#!/usr/bin/perl
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

sub showmenu() {
    print <<EOF
                <ul class='nav'>
EOF
;
    foreach my $k1 ( sort keys %$menu ) {
        if (! $menu->{$k1}{'enabled'}) {
            next;
        }
        my $link = getlink($menu->{$k1});
        if ($link eq '') {
            next;
        }
        if (! is_menu_visible($link)) {
            next;
        }
        if ($menu->{$k1}->{'selected'}) {
            print "<li><a href=\"$link\" class=\"active\">$menu->{$k1}{'caption'}</a></li>";
        } else {
            print "<li><a href=\"$link\">$menu->{$k1}{'caption'}</a></li>";
        }
    }
    print <<EOF
                </ul>
EOF
;
}

sub getselected($) {
    my $root = shift;
    if (!$root) {
        return 0;
    }

    foreach my $item (%$root) {
        if ($root->{$item}{'selected'}) {
            return $root->{$item};
        }
    }
}

sub showsubsection($$) {
    my $root = shift;

    if (! $root) {
        return;
    }
    my $selected = getselected($root);
    if (! $selected) {
        return;
    }
    my $submenus = $selected->{'subMenu'};
    if (! $submenus) {
        return;
    }

    print <<EOF

        <ul class="nav nav-list">
        <li class="nav-header">Sidemenu</li>
EOF
;
    foreach my $item (sort keys %$submenus) {
        my $hash = $submenus->{$item};
        if (! $hash->{'enabled'}) {
            next;
        }
        my $link = getlink($hash);
        if ($link eq '') {
            next;
        }
        if (! is_menu_visible($link)) {
            next;
        }
        if ($hash->{'selected'}) {
            print '<li class="active">';
        } else {
            print '<li>';
        }

        print "<a href=\"$link\">$hash->{'caption'}</a></li>";
    }

    print <<EOF
        </ul>
EOF
;
}


sub showsubsubsection($) {
    my $root = shift;
    if (!$root) {
        return;
    }
    my $selected = getselected($root);
    if (! $selected) {
        return
    }
    if (! $selected->{'subMenu'}) {
        return
    }

    showsubsection($selected->{'subMenu'}, 'menu-subtop');
}

sub openpage {
    my $title = shift;
    my $boh = shift;
    my $extrahead = shift;

    @URI=split ('\?',  $ENV{'REQUEST_URI'} );
    &General::readhash("${swroot}/main/settings", \%settings);
    &genmenu();

    my $h2 = gettitle($menu);

    $title = "IPFire: $title";
    if ($settings{'WINDOWWITHHOSTNAME'} eq 'on') {
        $title =  "$settings{'HOSTNAME'}.$settings{'DOMAINNAME'} - $title";
    }

    print <<END
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE html>
<html>
<head>
		<title>$title</title>
    $extrahead
END
;
    if ($settings{'FX'} ne 'off') {
    print <<END
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.5,Transition=12)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.5,Transition=12)" />
END
;
    }
    print <<END
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/themes/bootstrap/include/style.css" />
    <script language="javascript" type="text/javascript">

        function swapVisibility(id) {
            el = document.getElementById(id);
            if(el.style.display != 'block') {
                el.style.display = 'block'
            }
            else {
                el.style.display = 'none'
            }
        }
    </script>
END
;
if ($settings{'SPEED'} ne 'off') {
print <<END
    <script type="text/javascript" src="/include/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/themes/bootstrap/include/bootstrap.min.js"></script>
    <script type="text/javascript">
        var t_current;
        var t_last;
        var rxb_current;
        var rxb_last;
        var txb_current;
        var txb_last;
	function refreshInetInfo() {
		\$.ajax({
                	url: '/cgi-bin/speed.cgi',
                        success: function(xml){
                        t_current = new Date();
                        var t_diff = t_current - t_last;
                        t_last = t_current;

                        rxb_current = \$("rxb",xml).text();
                        var rxb_diff = rxb_current - rxb_last;
                        rxb_last = rxb_current;

                        var rx_kbs = rxb_diff/t_diff;
                        rx_kbs = Math.round(rx_kbs*10)/10;

                        txb_current = \$("txb",xml).text();
                        var txb_diff = txb_current - txb_last;
                        txb_last = txb_current;

                        var tx_kbs = txb_diff/t_diff;
                        tx_kbs = Math.round(tx_kbs*10)/10;

                        \$("#rx_kbs").text(rx_kbs + ' kb/s');
                        \$("#tx_kbs").text(tx_kbs + ' kb/s');
                        }
        	});
                window.setTimeout("refreshInetInfo()", 3000);
	}
	\$(document).ready(function(){
		refreshInetInfo();
	});
    </script>
  </head>
  <body>
END
;
}
else {
print "</head><body>";}
print ;
print <<END
<!-- IPFIRE HEADER -->

<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
END
;
    if ($settings{'WINDOWWITHHOSTNAME'} eq 'on') {
        print "<a class='brand' href='#'>$settings{'HOSTNAME'}.$settings{'DOMAINNAME'}: $h2</a>";
    } else {
                    print "<a class='brand' href='#'>IPFire&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>";
    }
    &showmenu();

                print <<END

    </div>

END
;

print <<END
</div>

<div id="main" style="margin-top: 40px;">
        <div id="main_inner" class="row">
            <div class="span2">
END
;
    &showsubsection($menu);
    &showsubsubsection($menu);
    $subt = gettitle($menu);
    print <<END
            </div>
            <div id="primaryContent_2columns" class="span10">
                <div id="columnA_2columns">
                    <ul class="breadcrumb">
                    <li><a href="/cgi-bin/index.cgi">Home</a> <span class="divider">/</span></li>
                    <li class="active">$subt</li>
                    </ul>
END
;
}

sub openpagewithoutmenu {
    my $title = shift;
    my $boh = shift;
    my $extrahead = shift;

    @URI=split ('\?',  $ENV{'REQUEST_URI'} );
    &General::readhash("${swroot}/main/settings", \%settings);
    &genmenu();

    my $h2 = gettitle($menu);

    $title = "IPFire: $title";
    if ($settings{'WINDOWWITHHOSTNAME'} eq 'on') {
        $title =  "$settings{'HOSTNAME'}.$settings{'DOMAINNAME'} - $title";
    }

    print <<END
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE html>
<html>
<head>
		<title>$title</title>
		$extrahead
END
;
    if ($settings{'FX'} ne 'off') {
    print <<END
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.5,Transition=12)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.5,Transition=12)" />
END
;
    }
    print <<END
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="/include/style.css" />
    <script language="javascript" type="text/javascript">

        function swapVisibility(id) {
            el = document.getElementById(id);
            if(el.style.display != 'block') {
                el.style.display = 'block'
            }
            else {
                el.style.display = 'none'
            }
        }
    </script>

  </head>
  <body>
<!-- IPFIRE HEADER -->

<div id="header">

        <div id="header_inner" class="fixed">

                <div id="logo">
                        <h1><span>IPFire</span></h1>
                        <h2>$h2</h2>
                </div>
        </div>
</div>

<div id="main">
        <div id="main_inner" class="row">
                <div class="span2"></div>

                <div id="primaryContent_2columns" class="span10">
                        <div id="columnA_2columns">
END
;
}

sub closepage () {
	my $status = &connectionstatus();
	my $uptime = `/usr/bin/uptime|cut -d \" \" -f 4-`;
	$uptime =~ s/year(s|)/$Lang::tr{'year'}/;
	$uptime =~ s/month(s|)/$Lang::tr{'month'}/;
	$uptime =~ s/day(s|)/$Lang::tr{'day'}/;
	$uptime =~ s/user(s|)/$Lang::tr{'user'}/;
	$uptime =~ s/load average/$Lang::tr{'uptime load average'}/;

    print <<END
                        </div>
                </div>
END
;

    print <<END
        <div class="clearfix"></div>
		<footer id="footer" class="footer">
			<b>Status:</b> $status <b>Uptime:</b> $uptime
END
;

if ($settings{'SPEED'} ne 'off') {
print <<END
                        <br />
                                <b>$Lang::tr{'bandwidth usage'}:</b>
				$Lang::tr{'incoming'}: <span id="rx_kbs" class="label label-success"></span>&nbsp;$Lang::tr{'outgoing'}: <span id="tx_kbs" class="label label-important"></span>

END
;
}
print <<END
                </footer>
        </div>
</div>
</body>
</html>
END
;
}

sub openbigbox
{
}

sub closebigbox
{
}

sub openbox
{
        $width = $_[0];
        $align = $_[1];
        $caption = $_[2];

        print <<END
<!-- openbox -->
        <div class="post" align="$align">
END
;

        if ($caption) { print "<h3>$caption</h3>\n"; } else { print "&nbsp;"; }
}

sub closebox
{
        print <<END
        </div>
        <div class="clearfix"></div>
        <!-- closebox -->
END
;
}

1;
