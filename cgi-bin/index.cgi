#!/usr/bin/perl
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2012  IPFire Team  <info@ipfire.org>                     #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

use strict;
use Net::Telnet;

# enable only the following on debugging purpose
#use warnings;
#use CGI::Carp 'fatalsToBrowser';

require '/var/ipfire/general-functions.pl';
require "${General::swroot}/lang.pl";
require "${General::swroot}/header.pl";
require "/opt/pakfire/lib/functions.pl";

my %cgiparams=();
my %pppsettings=();
my %modemsettings=();
my %netsettings=();
my %ddnssettings=();
my $warnmessage = '';
my $refresh = "";
my $ipaddr='';


&Header::showhttpheaders();

$cgiparams{'ACTION'} = '';
&Header::getcgihash(\%cgiparams);
$pppsettings{'VALID'} = '';
$pppsettings{'PROFILENAME'} = 'None';
&General::readhash("${General::swroot}/ppp/settings", \%pppsettings);
&General::readhash("${General::swroot}/modem/settings", \%modemsettings);
&General::readhash("${General::swroot}/ethernet/settings", \%netsettings);
&General::readhash("${General::swroot}/ddns/settings", \%ddnssettings);

my %color = ();
my %mainsettings = ();
&General::readhash("${General::swroot}/main/settings", \%mainsettings);
&General::readhash("/srv/web/ipfire/html/themes/".$mainsettings{'THEME'}."/include/colors.txt", \%color);

my $connstate = &Header::connectionstatus();

	if ( -e "/var/ipfire/main/gpl-accepted" ) {
if ($cgiparams{'ACTION'} eq $Lang::tr{'shutdown'} || $cgiparams{'ACTION'} eq $Lang::tr{'reboot'}) {
	$refresh = "<meta http-equiv='refresh' content='300;'>";
} elsif ($connstate =~ /$Lang::tr{'connecting'}/ || /$Lang::tr{'connection closed'}/ ){
	$refresh = "<meta http-equiv='refresh' content='5;'>";
} elsif ($connstate =~ /$Lang::tr{'dod waiting'}/ || -e "${General::swroot}/main/refreshindex") {
	$refresh = "<meta http-equiv='refresh' content='30;'>";
}
}

if ($cgiparams{'ACTION'} eq $Lang::tr{'dial profile'})
{
	my $profile = $cgiparams{'PROFILE'};
	my %tempcgiparams = ();
	$tempcgiparams{'PROFILE'} = '';
	&General::readhash("${General::swroot}/ppp/settings-$cgiparams{'PROFILE'}",
		\%tempcgiparams);

	# make a link from the selected profile to the "default" one.
	unlink("${General::swroot}/ppp/settings");
	link("${General::swroot}/ppp/settings-$cgiparams{'PROFILE'}",
		"${General::swroot}/ppp/settings");
	system ("/usr/bin/touch", "${General::swroot}/ppp/updatesettings");

	# read in the new params "early" so we can write secrets.
	%cgiparams = ();
	&General::readhash("${General::swroot}/ppp/settings", \%cgiparams);
	$cgiparams{'PROFILE'} = $profile;
	$cgiparams{'BACKUPPROFILE'} = $profile;
	&General::writehash("${General::swroot}/ppp/settings-$cgiparams{'PROFILE'}",
		\%cgiparams);

	# write secrets file.
	open(FILE, ">/${General::swroot}/ppp/secrets") or die "Unable to write secrets file.";
	flock(FILE, 2);
	my $username = $cgiparams{'USERNAME'};
	my $password = $cgiparams{'PASSWORD'};
	print FILE "'$username' * '$password'\n";
	chmod 0600, "${General::swroot}/ppp/secrets";
	close FILE;

	&General::log("$Lang::tr{'profile made current'} $tempcgiparams{'PROFILENAME'}");
	$cgiparams{'ACTION'} = "$Lang::tr{'dial'}";
}

if ($cgiparams{'ACTION'} eq $Lang::tr{'dial'}) {
	system('/usr/local/bin/redctrl start > /dev/null') == 0
	or &General::log("Dial failed: $?"); sleep 1;}
elsif ($cgiparams{'ACTION'} eq $Lang::tr{'hangup'}) {
	system('/usr/local/bin/redctrl stop > /dev/null') == 0
	or &General::log("Hangup failed: $?"); sleep 1;}

my $c;
my $maxprofiles = 5;
my @profilenames = ();

for ($c = 1; $c <= $maxprofiles; $c++)
{
	my %temppppsettings = ();
	$temppppsettings{'PROFILENAME'} = '';
	&General::readhash("${General::swroot}/ppp/settings-$c", \%temppppsettings);
	$profilenames[$c] = $temppppsettings{'PROFILENAME'};
}
my %selected;
for ($c = 1; $c <= $maxprofiles; $c++) {
	$selected{'PROFILE'}{$c} = '';
}
$selected{'PROFILE'}{$pppsettings{'PROFILE'}} = "selected='selected'";
my $dialButtonDisabled = "disabled='disabled'";


&Header::openpage($Lang::tr{'main page'}, 1, $refresh);
&Header::openbigbox('', 'center');

# licence agreement
if ($cgiparams{'ACTION'} eq $Lang::tr{'yes'} && $cgiparams{'gpl_accepted'} eq '1') {
	system('touch /var/ipfire/main/gpl_accepted')
}
if ( -e "/var/ipfire/main/gpl_accepted" ) {
&Header::openbox('100%', 'center', &Header::cleanhtml(`/bin/uname -n`,"y"));


if ( ( $pppsettings{'VALID'} eq 'yes' && $modemsettings{'VALID'} eq 'yes' ) || ( $netsettings{'CONFIG_TYPE'} =~ /^(1|2|3|4)$/ && $netsettings{'RED_TYPE'} =~ /^(DHCP|STATIC)$/ )) {
	if (open(IPADDR,"${General::swroot}/ddns/ipcache")) {
   	    $ipaddr = <IPADDR>;
    	    close IPADDR;
    	    chomp ($ipaddr);
	}
	if (open(IPADDR,"${General::swroot}/red/local-ipaddress")) {
	    $ipaddr = <IPADDR>;
	    close IPADDR;
	    chomp ($ipaddr);
	}
} elsif ($modemsettings{'VALID'} eq 'no') {
	print "$Lang::tr{'modem settings have errors'}\n </b></font>\n";
} else {
	print "$Lang::tr{'profile has errors'}\n </b></font>\n";
}

#if ( $netsettings{'RED_TYPE'} =~ /^(DHCP|STATIC)$/ ) {
#	$ipaddr = $netsettings{'RED_ADDRESS'};
#}

my $death = 0;
my $rebirth = 0;

if ($cgiparams{'ACTION'} eq $Lang::tr{'shutdown'}) {
	$death = 1;
	&General::log($Lang::tr{'shutting down ipfire'});
	system '/usr/local/bin/ipfirereboot down';
} elsif ($cgiparams{'ACTION'} eq $Lang::tr{'reboot'}) {
	$rebirth = 1;
	&General::log($Lang::tr{'rebooting ipfire'});
	system '/usr/local/bin/ipfirereboot boot';
}

if ($death == 0 && $rebirth == 0) {

if ($mainsettings{'REBOOTQUESTION'} eq "off") {
print <<END
	<form method='post' action='$ENV{'SCRIPT_NAME'}'>
	<table width='100%'>
	<tr>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'reboot'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-primary' name='ACTION' value='$Lang::tr{'refresh'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'shutdown'}' /></td>
	</tr>
	</table>
	</form>
END
;
} else {
	if ($cgiparams{'ACTION'} eq $Lang::tr{'reboot ask'}) {
print <<END
	<form method='post' action='$ENV{'SCRIPT_NAME'}'>
	<table width='100%'>
	  <tr>
	    <td colspan="3" align='left'><div style='color: #b94a48;' class='alert alert-error'>$Lang::tr{'reboot sure'}</div></td>
	    </tr>
	  <tr>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'reboot'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-primary' name='ACTION' value='$Lang::tr{'refresh'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'shutdown ask'}' /></td>
	</tr>
	</table>
	</form>
END
;
	} elsif ($cgiparams{'ACTION'} eq $Lang::tr{'shutdown ask'}) {
print <<END
	<form method='post' action='$ENV{'SCRIPT_NAME'}'>
	<table width='100%'>
	  <tr>
	    <td colspan="3" align='right'><div style='color: #b94a48;' class='alert alert-error'>$Lang::tr{'shutdown sure'}</div></td>
	    </tr>
	  <tr>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'reboot ask'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-primary' name='ACTION' value='$Lang::tr{'refresh'}' /></td>
		<td width='33%' align='center'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'shutdown'}' /></td>
	</tr>
	</table>
	</form>
END
;
	} else {
print <<END
		<form method='post' action='$ENV{'SCRIPT_NAME'}'>
		<table width='80%'>
		<tr>
			<td width='33%' align='right'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'reboot ask'}' /></td>
			<td width='33%' align='center'><input type='submit' class='btn btn-primary' name='ACTION' value='$Lang::tr{'refresh'}' /></td>
			<td width='33%' align='left'><input type='submit' class='btn btn-danger' name='ACTION' value='$Lang::tr{'shutdown ask'}' /></td>
		</tr>
		</table>
		</form>
END
;
	}
}
print <<END;



<!-- Table of networks -->
<table border='0' width=100% class='table'>
  <tr>  <th bgcolor='$color{'color20'}'>$Lang::tr{'network'}</th>
        <th bgcolor='$color{'color20'}'>IP</th>
        <th bgcolor='$color{'color20'}'>$Lang::tr{'status'}</th></tr>
  <tr>
  		<td align='center' width='25%'>
  			<span class='label label-important' style='background-color: $Header::colourred;'>
  				<a href="/cgi-bin/pppsetup.cgi" style='color: #fff;'><font size='2'><strong>$Lang::tr{'internet'}</strong></font></a>
  			</span>
  		</td>
        <td width='30%' align='center'>$ipaddr </td>
        <td width='45%' align='center'>$connstate
END
if ( $netsettings{'RED_TYPE'} ne "STATIC" && $netsettings{'RED_TYPE'} ne "DHCP" ){
print `/usr/local/bin/dialctrl.pl show`;
print <<END;
        </td></tr>
        <tr><td colspan='2'>

		<form method='post' action='$ENV{'SCRIPT_NAME'}'>$Lang::tr{'profile'}:
			<select name='PROFILE'>
END
	for ($c = 1; $c <= $maxprofiles; $c++)
	{
		if ($profilenames[$c] ne '') {
			$dialButtonDisabled = "";
			print "\t<option value='$c' $selected{'PROFILE'}{$c}>$c. $profilenames[$c]</option>\n";
		}
	}
	$dialButtonDisabled = "disabled='disabled'" if (-e '/var/run/ppp-ipfire.pid' || -e "${General::swroot}/red/active");
	if ( ( $pppsettings{'VALID'} eq 'yes' ) || ( $netsettings{'CONFIG_TYPE'} =~ /^(1|2|3|4)$/ && $netsettings{'RED_TYPE'} =~ /^(DHCP|STATIC)$/ ) ) {
		print <<END;
				</select>
				<input type='submit' name='ACTION' value='$Lang::tr{'dial profile'}' $dialButtonDisabled />
			</form>
			<td align='center'>
				<table width='100%' border='0'>
					<tr>
					<td width='50%' align='right'>	<form method='post' action='$ENV{'SCRIPT_NAME'}'>
											<input type='submit' name='ACTION' value='$Lang::tr{'dial'}'>
										</form>
					</td>
					<td width='50%' align='left'>	<form method='post' action='$ENV{'SCRIPT_NAME'}'>
											<input type='submit' name='ACTION' value='$Lang::tr{'hangup'}'>
										</form>
					</td>
					</tr>
				</table>
END
	} else {
	print "$Lang::tr{'profile has errors'}\n </b></font>\n";
	}
}
	my $HOSTNAME = (gethostbyaddr(pack("C4", split(/\./, $ipaddr)), 2))[0];
	if ( "$HOSTNAME" ne "" ) {
		print <<END;
	<tr><td><b>Hostname:</b><td align='center'>$HOSTNAME<td>&nbsp;
END
	}

	if ( -e "/var/ipfire/red/remote-ipaddress" ) {
		my $GATEWAY = `cat /var/ipfire/red/remote-ipaddress`;
		chomp($GATEWAY);
		print <<END;
	<tr><td><b>Gateway:</b><td align='center'>$GATEWAY<td>&nbsp;
END
	}

	my $DNS1 = `cat /var/ipfire/red/dns1`;
	my $DNS2 = `cat /var/ipfire/red/dns2`;
	chomp($DNS1);
	chomp($DNS1);

	if ( $DNS1 ) { print <<END;
	<tr><td><b>DNS-Server:</b><td align='center'>$DNS1
END
	}
	if ( $DNS2 ) { print <<END;
	<td align='center'>$DNS2
END
	} else { print <<END;
	<td>&nbsp;
END
	}

	if ( $netsettings{'GREEN_DEV'} ) { print <<END;
		<tr>
			<td align='center' width='25%'>
				<span class='label label-success' style='background-color: $Header::colourgreen;'>
					<a href="/cgi-bin/dhcp.cgi" style='color: #fff;'><font size='2'><strong>$Lang::tr{'lan'}</strong></font></a>
				</span>
			</td>
	  		<td width='30%' align='center'>$netsettings{'GREEN_ADDRESS'}</td>
  			<td width='45%' align='center'>
END
		if ( `cat /var/ipfire/proxy/advanced/settings | grep ^ENABLE=on` ) {
			print "<span class='label label-success'>$Lang::tr{'advproxy on'}";
			if ( `cat /var/ipfire/proxy/advanced/settings | grep ^TRANSPARENT=on` ) { print " (transparent)"; }
		}	else { print "<span class='label'>$Lang::tr{'advproxy off'}";  }
		print "</span>";
	}
print <<END;
			</td>
		</tr>
END
	if ( $netsettings{'BLUE_DEV'} ) { print <<END;
		<tr>
			<td align='center' width='25%'>
				<span class='label label-info' style='background-color: $Header::colourblue;'>
				<a href="/cgi-bin/wireless.cgi" style='color: #fff;'><font size='2'><strong>$Lang::tr{'wireless'}</strong></font></a>
				</span>
			</td>
	  	<td width='30%' align='center'>$netsettings{'BLUE_ADDRESS'}</td>
  		<td width='45%' align='center'>
END
		if ( `cat /var/ipfire/proxy/advanced/settings | grep ^ENABLE_BLUE=on` ) {
			print "<span class='label label-success' style='background-color: $Header::colourgreen;'>$Lang::tr{'advproxy on'}";
			if ( `cat /var/ipfire/proxy/advanced/settings | grep ^TRANSPARENT_BLUE=on` ) { print " (transparent)"; }
			print "</span>";
		}	else { print "<span class='label'>$Lang::tr{'advproxy off'}</span>";  }
	}
print <<END;
			</td>
		</tr>
END
	if ( $netsettings{'ORANGE_DEV'} ) { print <<END;
		<tr>
			<td align='center' width='25%'>
				<span class='label label-warning' style='background-color: $Header::colourorange;'>
				<a href="/cgi-bin/dmzholes.cgi" style='color: #fff;'><font size='2'><strong>$Lang::tr{'dmz'}</strong></font></a>
				</span>
			</td>
	  		<td width='30%' align='center'>$netsettings{'ORANGE_ADDRESS'}</td>
  			<td width='45%' align='center'><span class='label label-success' color=$Header::colourgreen>Online</span>
END
	}
print <<END;
			</td>
		</tr>
END
	if ( `cat /var/ipfire/vpn/settings | grep ^ENABLED=on` ||
	     `cat /var/ipfire/vpn/settings | grep ^ENABLED_BLUE=on` ) {
		my $ipsecip = `cat /var/ipfire/vpn/settings | grep ^VPN_IP= | cut -c 8-`;
		my @status = `/usr/local/bin/ipsecctrl I`;
		my %confighash = ();
		&General::readhasharray("${General::swroot}/vpn/config", \%confighash);
		print <<END;
		<tr>
			<td align='center' width='25%'>
				<span class='label label-info' style='background-color: $Header::colourvpn;'>
					<a href="/cgi-bin/vpnmain.cgi" style='color: #fff;'><font size='2'><strong>$Lang::tr{'vpn'}</strong></font></a>
				</span>
			</td>
	  		<td width='30%' align='center'>$ipsecip</td>
  			<td width='45%' align='center'>
  				<span class='label label-success' color=$Header::colourgreen>Online</span>
END
		my $id = 0;
		my $gif;
		foreach my $key (sort { uc($confighash{$a}[1]) cmp uc($confighash{$b}[1]) } keys %confighash) {
			if ($confighash{$key}[0] eq 'on') { $gif = 'on.gif'; } else { $gif = 'off.gif'; }

			if ($id % 2) {
          print "<tr><td align='left' nowrap='nowrap' bgcolor='$color{'color20'}'>$confighash{$key}[1] / " . $Lang::tr{"$confighash{$key}[3]"} . " (" . $Lang::tr{"$confighash{$key}[4]"} . ")</td><td align='center'>$confighash{$key}[11]</td>";
			} else {
          print "<tr></td><td align='left' nowrap='nowrap' bgcolor='$color{'color22'}'>$confighash{$key}[1] / " . $Lang::tr{"$confighash{$key}[3]"} . " (" . $Lang::tr{"$confighash{$key}[4]"} . ")</td><td align='center'>$confighash{$key}[11]</td>";
			}

			my $active = "<span class='label label-important' style='background-color: ${Header::colourred}; color: #FFF;'><strong>$Lang::tr{'capsclosed'}</strong></span>";
			if ($confighash{$key}[0] eq 'off') {
			    $active = "<span class='label label-info' style='background-color: ${Header::colourblue}; color: #FFF;'><strong>$Lang::tr{'capsclosed'}</strong></span>";
			} else {
			    foreach my $line (@status) {
				if (($line =~ /\"$confighash{$key}[1]\".*IPsec SA established/) ||
				    ($line =~/$confighash{$key}[1]\{.*INSTALLED/ ))
				    {
				    $active = "<span class='label label-success' style='background-color: ${Header::colourgreen}; color: #FFF;'><strong>$Lang::tr{'capsopen'}</strong></span>";
				}
			   }
			}
			print "<td align='center'>$active</td>";
		}
	}

###
# Check if there is any OpenVPN connection configured.
###

if ( -s "${General::swroot}/ovpn/ovpnconfig")
	{
	print <<END;

	<tr>
		<td align='center' width='25%'>
			<span class='label label-info' style='background-color: $Header::colourovpn;'>
				<a href="/cgi-bin/ovpnmain.cgi" style='color: #fff;'><font size='2'><strong>OpenVPN</strong></font></a>
			</span>
		</td>
END
	# Check if the OpenVPN server for Road Warrior Connections is running and display status information.
	my %confighash=();

	&General::readhash("${General::swroot}/ovpn/settings", \%confighash);

	if (($confighash{'ENABLED'} eq "on") ||
	    ($confighash{'ENABLED_BLUE'} eq "on") ||
	    ($confighash{'ENABLED_ORANGE'} eq "on")) {

		my $ovpnip = $confighash{'DOVPN_SUBNET'};
		print <<END;
	  	<td width='30%' align='center'>$ovpnip</td>
  		<td width='45%' align='center'>
  			<span class='label label-success' style='background-color: $Header::colourgreen;'>Online</span>
  		</td>
  	</tr>
END

	}

	# Print the OpenVPN N2N connection status.
	if ( -d "${General::swroot}/ovpn/n2nconf") {
		my %confighash=();

		&General::readhasharray("${General::swroot}/ovpn/ovpnconfig", \%confighash);
		foreach my $dkey (keys %confighash) {
			if (($confighash{$dkey}[3] eq 'net') && (-e "/var/run/$confighash{$dkey}[1]n2n.pid")) {
				my $tport = $confighash{$dkey}[22];
				next if ($tport eq '');

				my $tnet = new Net::Telnet ( Timeout=>5, Errmode=>'return', Port=>$tport);
				$tnet->open('127.0.0.1');
				my @output = $tnet->cmd(String => 'state', Prompt => '/(END.*\n|ERROR:.*\n)/');
				my @tustate = split(/\,/, $output[1]);

				my $display;
				my $display_colour = $Header::colourred;
				if ( $tustate[1] eq 'CONNECTED') {
					$display_colour = $Header::colourgreen;
					$display = "<span class='label label-success' style='background-color: $Header::colourgreen; color: #FFF;'>$Lang::tr{'capsopen'}</span>";
				} else {
					$display = $tustate[1];
				}

				print <<END;
				<tr>
					<td align='left' nowrap='nowrap' bgcolor='$color{'color22'}'>
						<span class='label label-info' style='background-color: $Header::colourovpn;'>
							$confighash{$dkey}[1]
						</span>
					</td>
					<td align='center'>
						$confighash{$dkey}[11]
					</td>
					<td align='center'>
						<strong>$display</strong>
					</td>
				</tr>
END
			}
		}
	}
}

# Fireinfo
if ( ! -e "/var/ipfire/main/send_profile") {
	$warnmessage .= "<li><a style='color: white;' href='fireinfo.cgi'>$Lang::tr{'fireinfo please enable'}</a></li>";
}

# Memory usage warning
my @free = `/usr/bin/free`;
$free[1] =~ m/(\d+)/;
my $mem = $1;
$free[2] =~ m/(\d+)/;
my $used = $1;
my $pct = int 100 * ($mem - $used) / $mem;
if ($used / $mem > 90) {
	$warnmessage .= "<li> $Lang::tr{'high memory usage'}: $pct% !</li>\n";
}

# Diskspace usage warning
my @temp=();
my $temp2=();
my @df = `/bin/df -B M -P -x rootfs`;
foreach my $line (@df) {
	next if $line =~ m/^Filesystem/;
	if ($line =~ m/root/ ) {
		$line =~ m/^.* (\d+)M.*$/;
		@temp = split(/ +/,$line);
		if ($1<5) {
			# available:plain value in MB, and not %used as 10% is too much to waste on small disk
			# and root size should not vary during time
			$warnmessage .= "<li> $Lang::tr{'filesystem full'}: $temp[0] <b>$Lang::tr{'free'}=$1M</b> !</li>\n";
		}

	} else {
		# $line =~ m/^.* (\d+)m.*$/;
		$line =~ m/^.* (\d+)\%.*$/;
		if ($1>90) {
			@temp = split(/ /,$line);
			$temp2=int(100-$1);
			$warnmessage .= "<li> $Lang::tr{'filesystem full'}: $temp[0] <b>$Lang::tr{'free'}=$temp2%</b> !</li>\n";
		}
	}
}

# S.M.A.R.T. health warning
my @files = `/bin/ls /var/run/smartctl_out_hddtemp-* 2>/dev/null`;
foreach my $file (@files) {
	chomp ($file);
	my $disk=`echo $file | cut -d"-" -f2`;
	chomp ($disk);
	if (`/bin/grep "SAVE ALL DATA" $file`) {
		$warnmessage .= "<li> $Lang::tr{'smartwarn1'} /dev/$disk $Lang::tr{'smartwarn2'} !</li>\n\n";
	}
}

# Reiser4 warning
my @files = `mount | grep " reiser4 (" 2>/dev/null`;
foreach my $disk (@files) {
	chomp ($disk);
	$warnmessage .= "<li>$disk - $Lang::tr{'deprecated fs warn'}</li>\n\n";
}


if ($warnmessage) {
	print "<tr><td align='center' bgcolor=$Header::colourred colspan='3'><font color='white'>$warnmessage</font></table>";
}
print <<END;
</table>
END
;
&Pakfire::dblist("upgrade", "notice");
print <<END;
END
if ( -e "/var/run/need_reboot" ) {
	print "<br /><br /><font color='red'>$Lang::tr{'needreboot'}!</font>";
}
} else {
	my $message='';
	if ($death) {
		$message = $Lang::tr{'ipfire has now shutdown'};
	} else {
		$message = $Lang::tr{'ipfire has now rebooted'};
	}
	print <<END
<div align='center'>
<table width='100%' bgcolor='#ffffff'>
<tr><td align='center'>
<br /><br /><img src='/images/IPFire.png' /><br /><br /><br />
</td></tr>
</table>
<br />
<font size='6'>$message</font>
</div>
END
;

}

&Header::closebox();
}

else {
&Header::openbox('100%', 'left', $Lang::tr{'gpl license agreement'});
print <<END;
	$Lang::tr{'gpl please read carefully the general public license and accept it below'}.
	<br /><br />
END
;
if ( -e "/usr/share/doc/licenses/GPLv3" ) {
	print '<textarea rows=\'25\' cols=\'75\' readonly=\'true\'>';
	print `cat /usr/share/doc/licenses/GPLv3`;
	print '</textarea>';
}
else {
	print '<br /><a href=\'http://www.gnu.org/licenses/gpl-3.0.txt\' target=\'_blank\'>GNU GENERAL PUBLIC LICENSE</a><br />';
}
print <<END;
	<p>
		<form method='post' action='$ENV{'SCRIPT_NAME'}'>
			<input type='checkbox' name='gpl_accepted' value='1'/> $Lang::tr{'gpl i accept these terms and conditions'}.
			<br/ >
			<input type='submit' name='ACTION' value=$Lang::tr{'yes'} />
		</form>
	</p>
	<a href='http://www.gnu.org/licenses/translations.html' target='_blank'>$Lang::tr{'gpl unofficial translation of the general public license v3'}</a>

END

&Header::closebox();
}

&Header::closebigbox();
&Header::closepage();
